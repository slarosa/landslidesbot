LandslidesBot è un bot Telegram che fornisce informazioni sulla Frana più vicina sulla base della tua posizione geografica. Il BOT usa i dati dell'Autorità di Bacino della Regione Calabria scaricabili dal link: http://goo.gl/abrHKX
=========================
*Per il momento funziona solo eseguendolo dalla Console python di QGIS :)*

==========================
Disclaimer
==========================
*Il bot non è stato sviluppato a scopo di lucro ma per semplice passione verso gli argomenti trattati. Si tratta di una demo e non è ufficiale e l'autore declina da ogni responsbilità*

==========================
Demo
==========================

https://telegram.me/landslidesbot

==========================
Librerie usate
==========================

- `python-telegram-bot <https://github.com/python-telegram-bot/python-telegram-bot>`_
- `utm <https://github.com/Turbo87/utm>`_
- `PyQGIS API <http://qgis.org>`_

==========================
Licenza (MIT)
==========================

Copyright (c) 2016 Salvatore Larosa

Con la presente si concede, a chiunque ottenga una copia di questo software e dei file di documentazione associati (il "Software"), l'autorizzazione a usare gratuitamente il Software senza alcuna limitazione, compresi i diritti di usare, copiare, modificare, unire, pubblicare, distribuire, cedere in sottolicenza e/o vendere copie del Software, nonché di permettere ai soggetti cui il Software è fornito di fare altrettanto, alle seguenti condizioni:
L'avviso di copyright indicato sopra e questo avviso di autorizzazione devono essere inclusi in ogni copia o parte sostanziale del Software.

IL SOFTWARE VIENE FORNITO "COSÌ COM'È", SENZA GARANZIE DI ALCUN TIPO, ESPLICITE O IMPLICITE, IVI INCLUSE, IN VIA ESEMPLIFICATIVA, LE GARANZIE DI COMMERCIABILITÀ, IDONEITÀ A UN FINE PARTICOLARE E NON VIOLAZIONE DEI DIRITTI ALTRUI. IN NESSUN CASO GLI AUTORI O I TITOLARI DEL COPYRIGHT SARANNO RESPONSABILI PER QUALSIASI RECLAMO, DANNO O ALTRO TIPO DI RESPONSABILITÀ, A SEGUITO DI AZIONE CONTRATTUALE, ILLECITO O ALTRO, DERIVANTE DA O IN CONNESSIONE AL SOFTWARE, AL SUO UTILIZZO O AD ALTRE OPERAZIONI CON LO STESSO.