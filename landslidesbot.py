# -*- coding:utf-8 -*-
"""
****************************************************************************
landslidesbot.py
                      -------------------
begin                : 2016-02-10
copyright            : (C) 2016 by Salvatore Larosa
email                : lrssvtml (at) gmail (dot) com
****************************************************************************
"""

from qgis.core import QgsVectorLayer, QgsPoint, QgsRectangle, QgsApplication

import logging
import ConfigParser
import sys
import os

config = ConfigParser.ConfigParser()
config.read(os.path.expanduser('~/settings.ini'))
token = config.get('main', 'token')
root_path = config.get('path', 'root_path')
shp_path = config.get('path', 'shp_path')
libs_path = config.get('path', 'libs_path')

sys.path.append(libs_path)
# sys.path.append("/Users/larosa/dev/QGIS/build-master/output/python")
# sys.path.append("/Applications/QGIS.app/Contents/Resources/python")

import utm
from telegram import Updater, ParseMode

# Enable Logging
logging.basicConfig(
        filename=os.path.join(os.path.expanduser('~/error_bot.log')),
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.INFO)

logger = logging.getLogger("landslidesbot")
last_chat_id = 0


# QgsApplication.setPrefixPath("/usr/local", True)
#
# qgs = QgsApplication(sys.argv, False)
# qgs.initQgis()


def start(bot, update):
    bot.sendMessage(chat_id=update.message.chat_id,
                    text="Ciao, inviami la tua posizione ed io ti dirò se sei in Frana.")
    bot.setWebhook()


def location(bot, update):
    """ Get position """

    pos = update.message.location
    if pos is not None:
        me = (pos['latitude'], pos['longitude'])
        # xccord, ycoord = pos['latitude'], pos['longitude']
        txt = get_landslides_info(pos)
        bot.sendMessage(chat_id=update.message.chat_id, text=txt, parse_mode=ParseMode.MARKDOWN)
    else:
        pass


def get_landslides_info(pos):
    """ Get information for landslides by position """

    layer = QgsVectorLayer(os.path.join(shp_path, "frane_2011.shp"), "frane", "ogr")
    # xccord, ycoord = pos['latitude'], pos['longitude']
    xccord, ycoord, _, _ = utm.from_latlon(pos['latitude'], pos['longitude'])
    pnt = QgsPoint(xccord, ycoord)
    radius = 1
    rect = QgsRectangle(pnt.x() - radius,
                        pnt.y() - radius,
                        pnt.x() + radius,
                        pnt.y() + radius)
    layer.select(rect, False)

    for f in layer.selectedFeatures():
        comune = f["Comune"]
        peric = f["Pericolosi"]
        tipologia = f["Tipologia"]
        attivita = f["Stato_Atti"]
        id_frana = f["Id_Frana"]

    text = "_Coordinate (UTM-WGS84): E{}, N{}_\n" \
           "_Coordinate (WGS84): E{}, N{}_\n\n".format(xccord, ycoord, pos['longitude'], pos['latitude'])

    if layer.selectedFeatureCount() > 0:
        text += "_Informazioni Frana ID {}:_\n*Comune:* _{}_\n*Tipologia:* _{}_\n" \
                "*Attività:* _{}_\n*Pericolosità:* _{}_".format(id_frana, comune, tipologia, attivita, peric)
    else:
        text += "Nessuna Frana presente per questa posizione :)"

    return text


def any_message(bot, update):
    """ Print to console """

    # Save last chat_id to use in reply handler
    global last_chat_id
    last_chat_id = update.message.chat_id

    logger.info("New message\nFrom: %s\nchat_id: %d\nText: %s" %
                (update.message.from_user,
                 update.message.chat_id,
                 update.message.text))


def error(bot, update, error):
    """ Print error to console """
    logger.warn('Update %s caused error %s' % (update, error))


updater = Updater(token=token)
dispatcher = updater.dispatcher
dispatcher.addTelegramCommandHandler('start', start)
dispatcher.addTelegramMessageHandler(location)

dispatcher.addErrorHandler(error)
dispatcher.addTelegramRegexHandler('.*', any_message)

update_queue = updater.start_polling(poll_interval=0.1, timeout=10)

# updater.stop()
# QgsApplication.exitQgis()
